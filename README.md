# Powered By PDI API Documentation (V1)
A RESTful API for building applications on top of the PDI Trust data set.

## API Basics
### Base URL
* The API URL assigned to your application is unique to your application all of
your interactions with the API should take place via that URL. In addition the
URL itself is versioned in order to support roll-out of new functionality to our
partners without precipitating failures across the install base of powered by
applications.

*Your assigned production URL will be in the following format:*

    https://<application_name>.<partner_name>.pxpbb.net/v1

*Your endpoint for testing and development will have the form:*

    https://<application_name>-sandbox.<partner_name>.pxpbb.net/v1

*Examples:*

    https://cool-app.acme-inc.pxpbb.net/v1
    https://cool-app-sandbox.acme-inc.pxpbb.net/v1





### Multilingual support
All content returned from the API is language neutral at the present time. In
the future we expect to support multilingual content via the HTTP
`Accept-Language` header. The caller should indicate the preferred language
by specifying the appropriate [locale identifier](http://www.ietf.org/rfc/rfc1766.txt).




### Supported Verbs
In general the behavior of various HTTP verbs when used with this API are
as follows. Any non-standard behavior is described in the endpoint details:

* POST:   Create a new item.  The resulting object including its id will be
          provided in the response.
* PUT:    apply a delta to the item at the URI specified. The HTTP body
          supplies the change to be made.
* GET:    retrieve the item or collection at the specified URI, see specific
          endpoint for supported query string parameters
* DELETE: delete the item or collection at the specified URI





### Response Formats
*Note:* Currently only JSON is supported. XML may be added in a subsequent release.





### Paging Results
All of the collection end points support the following query string parameters
for paging:

* limit - max number of records to return
* after - indicates a forward page, this retrieves values after the supplied id
* before - indicates a reverse page, this retrieves values before the supplied id

*For example, retrieving OpportunityHistory` records 5 at a time after a previous page,
submit the following request:*
    
    GET /v1/users/<userId>/opportunity-history?limit=5&after=<id>

*Another example, retrieving PointsOfInterest` records 3 at a time after a previous page,
submit the following request:*
    
    GET /v1/users/<userId>/points-of-interest?type=<type>&lng=<lng>&lat=<lat>&distance=<distance>&limit=3



### Major Functional Areas
* [Authentication](#markdown-header-authentication)
* [Shopping lists](#markdown-header-shopping-lists)
* [User Registration and Enrollment](#markdown-header-user-registration-and-enrollment)
* [User Wallet](#markdown-header-user-wallet)
* [User Activity Tracking](#markdown-header-events-collection-endpoint)
* [Example Usage Sequences](#markdown-header-example-usage-sequences)



### Available End Points
* [/v1/accounts](#markdown-header-accounts-collection-endpoint)
* [/v1/accounts/<account-id>](#markdown-header-accounts-instance-endpoint)
* [/v1/accounts/<account-id>/transactions](#markdown-header-transactions-for-account-endpoint)
* [/v1/events](#markdown-header-events-collection-endpoint)
* [/v1/lists](#markdown-header-lists-collection-endpoint)
* [/v1/lists/<list-id>](#markdown-header-list-instance-endpoint)
* [/v1/lists/<list-id>/list-items](#markdown-header-list-items-endpoint)
* [/v1/products](#markdown-header-products-collection-endpoint)
* [/v1/profiles](#markdown-header-profiles-collection-endpoint)
* [/v1/sessions](#markdown-header-sessions-collection-endpoint)
* [/v1/shares](#markdown-header-shares-collection-endpoint)
* [/v1/taxonomies/list-categories](#markdown-header-list-categories-collection-endpoint)
* [/v1/taxonomies/product-categories](#markdown-header-product-categories-collection-endpoint)
* [/v1/users](#markdown-header-users-collection-endpoint)
* [/v1/users/<user-id>](#markdown-header-user-instance-endpoint)
* [/v1/users/<user-id>/accounts](#markdown-header-accounts-for-user-endpoint)
* [/v1/users/<user-id>/lists](#markdown-header-lists-for-user-endpoint)
* [/v1/users/<user-id>/opportunities](#markdown-header-opportunities-for-user-endpoint)
* [/v1/users/<user-id>/opportunity-history](#markdown-header-opportunity-history-for-user-endpoint)
* [/v1/users/<user-id>/profiles](#markdown-header-profiles-for-user-endpoint)
* [/v1/users/<user-id>/transactions](#markdown-header-transactions-for-user-endpoint)

### Object Definitions

* [Account](#markdown-header-account-object-read-only)
* [Category](#markdown-header-category-object-read-only)
* [Event](#markdown-header-event-object)
* [List](#markdown-header-list-object)
* [ListItem](#markdown-header-listitem-object)
* [Opportunity](#markdown-header-opportunity-object)
* [OpportunityHistory](#markdown-header-opportunity-history-object)
* [Profile](#markdown-header-profile-object)
* [Product](#markdown-header-product-object-read-only)
* [Share](#markdown-header-share-object)
* [Transaction](#markdown-header-transaction-object)
* [User](#markdown-header-user-object)





## End Point Details

go to [endpoint list](#markdown-header-available-end-points)
### Accounts Collection Endpoint
    /v1/accounts

This endpoint returns the set of financial accounts associated with the current
user. See also [User Wallet](#markdown-header-user-wallet)

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok`, with body containing an array of
           [Account](#markdown-header-account-object-read-only) objects.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Accounts Instance Endpoint
    /v1/accounts/<account-id>
    
This endpoint provides the details of a specific account belonging to the user.
In all cases the operation does not succeed if the currently logged in user is
not the owner of the specified [Account](#markdown-header-account-object-read-only).
     
##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok` with body containing the [Account](#markdown-header-account-object-read-only)
           object specified.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`

    



go to [endpoint list](#markdown-header-available-end-points)
### Transactions for Account Endpoint
    /v1/accounts/<account-id>/transactions

This endpoint is part of [User Wallet](#markdown-header-user-wallet) functionality and can be
used to retrieve the transaction activity for a specific account. The response
will be contain an array of [Transaction](#markdown-header-transaction-object) objects. Callers
should specify paging parameters when calling this endpoint. If no paging
parameters are provided then a default of `limit=50` will be used.

    Note: The paging defaults as described above are not yet implemented

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok` and body containing the 50 most recent transactions
           for the account specified by account-id.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`




go to [endpoint list](#markdown-header-available-end-points)
### Events Collection Endpoint
    /v1/events

This endpoint is used to record user activity in the database for targeting and
usage tracking purposes. For example: purchases, coupon redemption, ad views
etc... Refer to the [Event](#markdown-header-event-object) object for details including
supported event types.

    Note: These paging defaults described above are not yet implemented

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - `405 Method Not Allowed`
* POST   - Returns `201 Ok`, with body containing the new event upon
           successful storage of the event into the database.
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Lists Collection Endpoint
    /v1/lists

This endpoint is used to enable users to create and manage [Shopping lists](#markdown-header-shopping-lists).

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok`, with body containing an array of [List](#markdown-header-list-object)
           objects belonging to the current user.
* POST   - Returns `201 Ok`, with body containing the new [List](#markdown-header-list-object)
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### List Instance Endpoint
    /v1/lists/<list-id>

This endpoint provides the details of the items in a specific [List](#markdown-header-list-object).
Returns 403 Forbidden if the currently logged in user is not the owner of the
specified list.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok`, with body containing the specified list.
* POST   - `405 Method Not Allowed`
* PUT    - Replaces the existing list with the set of ListItem objects submitted in this request.
* DELETE - Returns `200 Ok`, and deletes the specified list.





go to [endpoint list](#markdown-header-available-end-points)
### List Items Endpoint
    /v1/lists/<list-id>/list-items

This endpoint is a convenience method used to add new items to an existing list.
Returns `403 Forbidden` if the currently logged in user is not the owner of the
specified list.


##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - `405 Method Not Allowed`
* POST   - Returns `201 Ok`, and adds a new [ListItem](#markdown-header-listitem-object) to the list
           specified in the URL.
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Products Collection Endpoint
    /v1/products?barCode=

This endpoint is used to search for products by UPC or EAN. If no matching product
is found then a response of `404 Not Found` is returned.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok`, with body containing the [Product](#markdown-header-product-object-read-only)
           object matching the specified barCode.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Profiles Collection Endpoint
    /v1/profiles

This endpoint provides read and write access to the user's profile as stored
by the application.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok`, with body containing the [Profile](#markdown-header-profile-object)
           object for the current user.
* POST   - `405 Method Not Allowed`. The [Profile](#markdown-header-profile-object) object is
           created when the [User](#markdown-header-user-object) object is created.
* PUT    - Modifies the [Profile](#markdown-header-profile-object) of the current user.
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Sessions Collection Endpoint
    /v1/sessions

This endpoint is used exclusively for authentication. The body of the POST
must be a JSON document containing `username`, `password`, `appId`. The
password must not be sent in clear text, encrypt the password before
transmission via the provided client side library. The appId is unique to
your application and assigned by PBB.

##### Responses by Verb
* GET    - `405 Method Not Allowed`
* POST   - Returns `201 Ok`, and with body containing a JWT token. See the
           section on [Authentication](#markdown-header-authentication) for details.
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Shares Collection Endpoint
    /v1/shares

This endpoint facilitates the sharing of [List](#markdown-header-list-object) objects and
ListItems between users. It may also be extended in the future to provide for
sharing of other object types. Currently posting to this endpoint creates a
share request which when accepted by the recipient has the side-effect of
deleting the [Share](#markdown-header-share-object) object. Shares only exist while a Share
request is pending.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok`, with a body containing an array of [Share](#markdown-header-share-object)
           objects for which the current user is the recipient.
* POST   - Return `201 Ok`, with a body containing ... the Create a new [Share](#markdown-header-share-object) specifying the recipient
* PUT    - The recipient uses this method to indicate acceptance or rejection
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### List Categories Collection Endpoint
    /v1/taxonomies/list-categories

This is a special purpose endpoint used to identify the type of list a user may
be creating and provide a corresponding icon set. The endpoint recognizes
certain keywords and returns canonical categories corresponding to the keyword.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns an array of [Category](#markdown-header-category-object-read-only) objects.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Product Categories Collection Endpoint
    /v1/taxonomies/product-categories

This is a special purpose endpoint used to search the product category
hierarchy.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns an array of [Category](#markdown-header-category-object-read-only) objects.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Users Collection Endpoint
    /v1/users

This endpoint is used to register new users or determine if a user has already
registered or not. The caller does not need to be authenticated to call this
endpoint. See also [User Registration and Enrollment](#markdown-header-user-registration-and-enrollment).

##### Responses by Verb
* GET    - Returns an [User](#markdown-header-user-object) object when a username query has been
           specified, otherwise 405 Method Not Allowed.
* POST   - Returns `201 Ok`, with body containing the newly created user unless
           the user already existed in which case `200 Ok` is returned with body
           containing the existing user.
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### User Instance Endpoint
    /v1/users/<user-id>

This endpoint returns the [User](#markdown-header-user-object) object specified in the URL.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns the requested [User](#markdown-header-user-object) if and only if the
           currently logged in user is the same as that requested.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Accounts For User Endpoint
    /v1/users/<user_id>/accounts

This is the set of financial accounts associated with the user. See also [User Wallet](#markdown-header-user-wallet)

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns an array of [Account](#markdown-header-account-object-read-only) objects if the
           currently logged in user is the same as that requested.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`





go to [endpoint list](#markdown-header-available-end-points)
### Lists For User Endpoint
    /v1/users/<user-id>/lists

This endpoint is identical in behavior as [/v1/lists](#markdown-header-lists-collection-endpoint)
with the exception that the user id is specified explicitly. If the current 
user is not the same as that specified a `403 Forbidden` is returned.






go to [endpoint list](#markdown-header-available-end-points)
### Opportunities For User Endpoint
    /users/<user_id>/opportunities

This endpoint returns an array of [Opportunity](#markdown-header-opportunity-object) objects.
These objects are readonly therefore only `GET` is supported.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok`, with a body containing an array of opportunities
           for the specified user. Returns `403 Forbidden` if the current user
           is not the same as specified in the URL.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`




go to [endpoint list](#markdown-header-endpoint-list)
### Opportunity History For User Endpoint
    /users/<user_id>/opportunity-history

This endpoint returns an array of [OpportunityHistory](#markdown-header-opportunity-history-object) objects.
These objects are readonly therefore only `GET` is supported.
Paging is supported using the `limit`, `after`, and `before` parameters.

##### Responses by Verb
* (all)  - Returns `401 Unauthorized` if caller is not authenticated.
* GET    - Returns `200 Ok`, with a body containing an array of opportunity history records
           for the specified user. Returns `403 Forbidden` if the current user
           is not the same as specified in the URL.
* POST   - `405 Method Not Allowed`
* PUT    - `405 Method Not Allowed`
* DELETE - `405 Method Not Allowed`




go to [endpoint list](#markdown-header-available-end-points)
### Profiles For User Endpoint
    /v1/users/<user-id>/profiles

This endpoint is identical in behavior as [/v1/profiles](#markdown-header-profiles-collection-endpoint)
with the exception that the user id is specified explicitly. If the current
user is not the same as that specified a `403 Forbidden` is returned.





go to [endpoint list](#markdown-header-available-end-points)
### Transactions For User Endpoint
    /v1/users/<user-id>/transactions

This endpoint is similar in behavior as [/v1/accounts/<account-id>/transactions](#markdown-header-profiles-collection-endpoint)
This endpoint returns paged results of all the transactions for all the user's
accounts governed by this powered by application. If the current user is not
the same as that specified a `403 Forbidden` is returned. See also [User Wallet](#markdown-header-user-wallet).





## Authentication
Most of the endpoints require that the user be authenticated before calling the
endpoint. To authenticate you must issue a POST request to the [sessions endpoint](#markdown-header-sessions-collection-endpoint)
The body of the POST must be a JSON object with the `username`, `password` and
`appId`. The password must not be sent in clear text, instead call the doHash
function from the [client-side library] (https://github.com/PersonalBlackBox/pxpbb-docs/blob/master/client-library.js).

The application id, secret and base URL are provided to powered by developers
at project commencement. The application id must be provided with the
authentication request as the value for the appId parameter.

Example POST content:

    {
       "username": "testuser@test.com",
       "password": "ENC!2avkWjz2Txf4Zhd8FNqiRQ==",
       "appId": "548caf3827de998c08d4500a"
    }

Successful authentication results in a response of 200 Ok with a response body
containing a JSON Web Token (JWT). The client should validate the JWT by
decoding it and confirming the issuer (`iss`) and audience (`aud`) values are
as expected.

All subsequent calls to the API for which authentication is required should
include an HTTP `Authorization` header like so:

    Authorization: Bearer <jwt-goes-here>;

A `Session expired` error will be thrown in response to any request made with
an expired token. Submit the user credentials again to obtain a new token.





## Shopping lists
The API provides several endpoints for the purpose of enabling shopping list
applications. The primary activities for list management are:

* See a list of shopping lists, call `GET` [/v1/lists](#markdown-header-lists-collection-endpoint)
* Create new list, call `POST` [/v1/lists](#markdown-header-lists-collection-endpoint)
  the only required attribute is `name`. The `categoryId` and `items` array are
  optional. The `items` array if provided should contain [ListItem](#markdown-header-listitem-object)
  objects.
* Add items to a list, call `POST` [/v1/lists/<list-id>/list-items](#markdown-header-list-items-endpoint),
  the body of the request should contain a single [ListItem](#markdown-header-listitem-object)
  which will be added to the list.
* Save changes to a list `PUT` [/v1/lists/<list-id>](#markdown-header-list-instance-endpoint),
  the body of the request should contain a [List](#markdown-header-list-object) with only the
  changed attributes present. The database copy will be merged with the
  provided object and persisted back to the database.
* Remove an item from a list, modify the `items` array and then save the list.
  See also [List](#markdown-header-list-object) object.
* Mark an item in a list as purchased, modify the list item and then save the
  list. See also [ListItem](#markdown-header-listitem-object) object.
* Rename a list, call `PUT` [/v1/lists/<list-id>](#markdown-header-list-instance-endpoint)
  with the new name: `{ "name" : "My New List Name" }`
* Delete a list, call `DELETE` [/v1/lists/<list-id>](#markdown-header-list-instance-endpoint)

Some additional features:

* Add a product to list via scanning its barcode, call `GET` [/v1/products](#markdown-header-products-collection-endpoint)
  with the `barCode` on the query string. Then include the `id` of the returned
  product in the `productId` attribute of a [ListItem](#markdown-header-listitem-object)
  object. Lastly, call `POST` [/v1/lists/<list-id>/list-items](#markdown-header-list-items-endpoint)
  to add this new ListItem to the list.
* Decorate a list with an appropriate icon, call `GET` [/v1/taxonomies/list-categories](#markdown-header-list-categories-collection-endpoint)
  passing a partial keyword or name for the list in the query string using the
  `name` parameter. The response will include a suggested category for the list
  and URL to the suggested icon.





## User Registration and Enrollment
The powered by API is used by multiple applications each is has a unique base
URL, appId and secret assigned. A user signing up for a powered by application
may have previously signed up for a different powered by application. Its in
the best interests of both PBB and the user that we recognize this and avoid
creating a duplicate account for the same individual. Consequently, the user
model stored in the database includes an attribute called `enrollments`. This
is an array of strings identifying the powered by applications the user has
registered for. _Note that the system requires the `username` attribute to be
an email address._ Whenever a registration is received by the API the system
checks to see if the  submitted username is already present in the database.
If not a new user is created and returned with status `201`. If the username
is already in use the system to check if the password matches the password on
file and if so add the new application to the enrollments array and status
`200` is returned. If the password doesn't match an error `500 User already
exists` is reported to the caller. The contents of the enrollments array is
considered private and is never returned on any [User](#markdown-header-user-object) object.






## User Wallet
The wallet functionality provides storage and management of loyalty points.
Additionally, the management of cash balances are also possible by special
arrangement. Each user's wallet is comprised of one or more accounts each
account is denominated in a particular point type or currency. Presently,
direct transactions against an account are prohibited; all credits and
debits against an account occur as a side effect of some other action. For
example a survey submission or video proof of play. We anticipate allowing
direct transactions via server to server communication with partners who
implement an register a secure server with us for this purpose. Also note
that since the user may be enrolled in multiple powered by applications
the user in effect has one wallet per application and all of the endpoints
described below are constrained to the current application. Currently
supported wallet functionality for a logged in user:

* See balances of all my accounts under this application, call
  `GET` [/v1/accounts](#markdown-header-accounts-collection-endpoint)
* See transaction history for all my accounts under this application, for
  each account call `GET` [/v1/accounts/<account-id>/transactions](#markdown-header-transactions-for-account-endpoint)






## Object Details

go to [object list](#markdown-header-object-definitions)
### Account Object (Read Only)
An Account is a bucket of stored value associated with a member and denominated
in a particular currency or loyalty point type.

##### Attributes
* accountType    - One of CREDIT or DEBIT, all accounts in the system are
                   currently debit accounts. Credit accounts may be made
                   available in the future.
* balance        - Current magnitude of value stored
* currency       - Currency code, ([list of valid currency codes](#markdown-header-valid-currency-codes))
* userId         - The user to whom the account belongs

##### Example
    {
       accountType : "DEBIT",
       balance     : "1000",
       currency    : "PBB",
       userId      : "5486036bada9d52f2fa8b7fb"
    }





go to [endpoint list](#markdown-header-available-end-points)
### Category Object (Read Only)
Categories define a taxonomy, currently there are 2 taxonomies in the system
list and product.

##### Attributes
name       - The name of this Category
aliases    - Array of strings that are consider valid alternate names for this Category
taxonomyId - The taxonomy to which this Category belongs

##### Example
    {
       name       : "Groceries",
       aliases    : ["Food","Grocery","Groceries"]
       taxonomyId : "lists"
    }





go to [endpoint list](#markdown-header-available-end-points)
### Event Object
Event objects are used to record many different kinds of disparate activity
and consequently the structure of Event objects are somewhat fluid. They all
share common attributes but there are differences based on the specific type
of event being recorded. The currently recognized event types are

 - APP_START
 - LOGIN (auto generated on the server)
 - OPPTY_VIEW
 - OPPTY_CLICK
 - OPPTY_SUBMIT
 - OPPTY_ITEM_VIEW
 - OPPTY_ITEM_CLICK

##### Attributes

Name     |Description
---------|-----------
eventType|(required) Indicates the type of event being recorded
userId   |The user who performed/experienced the event
actionId |Indicates the action to which the user was responding (coupon, video, survey)
details  |A JSON object specific to the application and eventType, discuss the precise details with your PBB representative.

##### Optional Attributes
These attributes may be submitted in the details object, subject to the
specific goals and constraints of your application. Please review with
your PBB representative.


Name     |Description
---------|-----------
amount        | Amount of a purchase
description   | Text description of the purchased item, including brand
deviceId      | Unique identifier for the user's device
coords        | Longitude / latitude coordinates of user's current location
localTime     | Current time as reported by the user's device in epoch time
quantity      | Number of items purchased
transactionId | Correlation value for identifying purchases occurring in the same transaction


##### Event Type Examples

###### APP_START
For some partners this event can be generated on the server. Speak to
your PBB representative if that is your preference.

    {
        eventType: 'APP_START',
        details: {
            coords: [<lng>,<lat>],
            deviceId: <device-id>,
            localTime: <in seconds since epoch>
        }
    }


###### LOGIN
This event is automatically generated on the server during the
normal login process, no need to submit an event of this type

    {
        eventType: 'LOGIN',
        userId: <user-id>,
        details: {
            coords: [<lng>,<lat>],
            deviceId: <device-id>,
            localTime: <in seconds since epoch>
        }
    }


###### OPPTY_VIEW, OPPTY_CLICK
These 2 event types are identical except for the eventType attribute.
Post the view event whenever the associated opportunity has been viewed
by the user. Post the click event when the user has interacted with the
opportunity. Be sure to discuss the nuances of these events with your
PBB representative as there are targeting implications.

    {
        eventType: 'OPPTY_VIEW',
        userId: <user-id>,
        actionId: <action-id>,
        details: {
            coords: [<lng>,<lat>],
            deviceId: <device-id>,
            localTime: <in seconds since epoch>
        }
    }


###### OPPTY_SUBMIT
This event type is use to indicate full user engagement with the
opportunity (i.e. video proof of play, survey submission, coupon
redemption). The example below shows a typical coupon redemption.
Note that the body of the details attribute is almost a verbatim
[opportunity object](#markdown-header-opportunity-object).

    {
        eventType: 'OPPTY_SUBMIT',
        userId: <user-id>,
        actionId: <action-id>,
        details: {
            coords: [<lng>,<lat>],
            deviceId: <device-id>,
            localTime: <in seconds since epoch>
            actionId: <action-id>,
            assetType: "CouponAsset",
            title: "20% Off Wild Oats Cereal",
            subtitle: "Save 20% off any size Wild Oats breakfast cereal",
            expiration: <date time in JS format>
            image: <url to image>
            barcodeFormat: "IMAGE",
            barcodeValue: <url to barcode image>,
            termsAndConditions: [
                "Not valid with any other offer",
                "Limit one coupon per customer",
                "Participating retailers"
            ]
        }
    }


go to [endpoint list](#markdown-header-available-end-points)
### List Object
List of items the member intends to buy

##### Attributes
* name           - (Required) Name of the list
* categoryId     - Product category of the list
* itemCount      - Computed field with number of items in the list
* isBuiltIn      - A built in list cannot be created or deleted by the member.
                   Ex. - Wish List, Saved Offers
* isCompleted    - Computed field returns true if all the items are complete.
* forkedFrom     - ListId of the original list from which this list was copied
* belongsTo      - (Required) Array of `id`s one for each user sharing the list
                   , always has at least one entry.
* items          - Array of ListItems





go to [endpoint list](#markdown-header-available-end-points)
### ListItem Object
Powered by application developers may add additional attributes to list items
the API will record them in the database and return them with the object in
any query results containing the list item.

##### Attributes
* isCompleted    - Is item completed (true/false)
* name           - (Required) Name of the item
* productId      - Corresponding Product in the product database
* quantity       - How many of the items are intended for purchase





go to [endpoint list](#markdown-header-available-end-points)
### Opportunity Object

##### Attributes
Name               |Description
-------------------|-----------
actionId           | The primary key of the marketer's targeting action which resulted in this opportunity
assetType          | One of CouponAsset, SurveyAsset, VideoAsset
title              | Title to display for this opportunity
subtitle           | Optional subtitle to display for this opportunity
expiration         | Last day the opportunity can be redeemed
image              | Image associated with the opportunity
barcodeFormat      | One of: "IMAGE", "EAN|8", "EAN|13", "EAN|14", "UPC|A", "UPC|E"
barcodeValue       | The numeric value of the bar code or url of barcode image
termsAndConditions | An array of strings detailing the terms and conditions of the opportunity


##### Example

    {
        actionId: <action-id>,
        assetType: "CouponAsset",
        title: "20% Off Wild Oats Cereal",
        subtitle: "Save 20% off any size Wild Oats breakfast cereal",
        expiration: <date time in JS format>
        image: <url to image>
        barcodeFormat: "IMAGE",
        barcodeValue: <url to barcode image>,
        termsAndConditions: [
            "Not valid with any other offer",
            "Limit one coupon per customer",
            "Participating retailers"
        ]
    }


go to [endpoint list](#markdown-header-available-end-points)
### Opportunity History Object

##### Attributes
Name             |Description
-----------------|-----------
userId           | The user to whom the history belongs
opportunityType  | One of CouponAsset, SurveyAsset, VideoAsset
createdOn        | Timestamp in UTC
details          | Sub document that contains the specifics of the original opportunity

##### Example

    {
        userId: <user-id>,
        opportunityType: "CouponAsset",
        createdOn: <datetime in JS format>,
        details: {
            title: "20% Off Wild Oats Cereal",
            subtitle: "Save 20% off any size Wild Oats breakfast cereal",
            expiration: <date time in JS format>
            image: <url to image>
            barcodeFormat: "IMAGE",
            barcodeValue: <url to barcode image>,
            termsAndConditions: [
                "Not valid with any other offer",
                "Limit one coupon per customer",
                "Participating retailers"
            ]
        }
    }


go to [endpoint list](#markdown-header-available-end-points)
### Profile Object
The user profile information is schema-less and returns a single JSON object
with the name value pairs. We have defined a set of [recommended profile attribute names](#markdown-header-recommended-profile-attribute-names)
. These recommended attributes are currently or will be enabled for targeting
in the marketing app that may appear in the profile and the encryption
rules we require for these data elements. In general all PII must be encrypted with the user's private key in the client
application before transmitting to PBB. Any profile data which is intended for targeting purposes should not be
encrypted with the user's key, however when user profile data is transmitted to PBB the entire profile object should
be encrypted with the key provided by the API. No profile data is ever to be transmitted in clear text. Transport
encryption via SSL is always required and is not sufficient to meet the previous requirement that profile data never be
transmitted in clear text.

##### Attributes
Defined by client application but expected to adhere to our [recommendations](#markdown-header-recommended-profile-attribute-names)





go to [endpoint list](#markdown-header-available-end-points)
### Product Object (Read Only)

##### Attributes
* name   - The product name
* upc    - The upc code
* weight - Size or weight of the product
* brand  - The product brand name
* manufacturer - The product manufacturer name
* categoryId - The [Category](#markdown-header-category-object-read-only) to which this product belongs
* Additional attributes may be present depending on the precise Product (i.e. - color, flavor, ingredients)





go to [endpoint list](#markdown-header-available-end-points)
### Share Object
A Share object is a temporary object that exists until the recipient accepts
the Share at which time added to the recipients belongings. If the share is
rejected by the recipient it is deleted.

##### Attributes (All Required)
* by       - The userId of the person sharing the item
* itemId   - Unique identifier for the item
* itemType - Indicates the address space to which the itemId belongs (List, Product or Opportunity)
             currently only 'List' is supported.
* with     - The intended recipient of the shared item





go to [endpoint list](#markdown-header-available-end-points)
### Transaction Object

##### Attributes





go to [endpoint list](#markdown-header-available-end-points)
### User Object

##### Attributes
* email     - The users email address
* password  - Hash of the users password
* username  - The username the user uses to login
* isEmailVerified - The email verification status


### Example Usage Sequences
#### Anonymous access
* Get the device id unless already available on device

`POST /v1/extensions/register-device`

    {
        "uuid"        : "b391a27a3eaf8132",
        "phoneNumber" : null,
        "platform"    : "Android"
    }

* Get list of locations with opportunities

`GET /v1/points-of-interest?lng=-112.254608&lat=33.479301&distance=2000`

* Get list of opportunities at current or selected location

`GET /v1/opportunities?lng=-112.254608&lat=33.479301&distance=2000`

* Tell us what opportunities were displayed to the user

`POST /v1/events`

    {
        eventType: 'OPPTY_VIEW',
        userId: null,
        actionId: <a unique action-id is on each opportunity>,
        details: {
            coords: [<users current lng>,<users current lat>],
            deviceId: <device-id>,
            localTime: <in seconds since epoch>
        }
    }

#### Register and use the app
* Get the device id unless already available on device

`POST /v1/extensions/register-device`

    {
        "uuid"        : "b391a27a3eaf8132",
        "phoneNumber" : null,
        "platform"    : "Android"
    }

* Create a new user

`POST /v1/users`

    {
        "username" : <user email address>,
        "password" : <hashed password>
    }

* Start new session

`POST /v1/sessions`

    {
        "username" : <user email address>,
        "password" : <hashed password>,
        "appId"    : <as assigned to your app>
    }


* Get list of locations with opportunities

`GET /v1/points-of-interest?lng=-112.254608&lat=33.479301&distance=2000`

* Get list of opportunities for the current user at a specific location

`GET /v1/users/<user-id>/opportunities?lng=-112.254608&lat=33.479301&distance=2000`

* Tell us what opportunities were displayed to the user

`POST /v1/events`

    {
        eventType: 'OPPTY_VIEW',
        userId: <current users id>,
        actionId: <a unique action-id is on each opportunity>,
        details: {
            coords: [<users current lng>,<users current lat>],
            deviceId: <device-id>,
            localTime: <in seconds since epoch>
        }
    }

* Tell us what opportunities the user viewed details about.

`POST /v1/events`

    {
        eventType: 'OPPTY_CLICK',
        userId: <current users id>,
        actionId: <a unique action-id is on each opportunity>,
        details: {
            coords: [<users current lng>,<users current lat>],
            deviceId: <device-id>,
            localTime: <in seconds since epoch>
        }
    }

* Tell us what opportunities the user redeemed.

`POST /v1/events`

    {
        eventType: 'OPPTY_SUBMIT',
        userId: <current users id>,
        actionId: <a unique action-id is on each opportunity>,
        details: {
            coords: [<users current lng>,<users current lat>],
            deviceId: <device-id>,
            localTime: <in seconds since epoch>
            actionId: <a unique action-id is on each opportunity (yes its redundant)>,
            assetType: "CouponAsset",
            title: "20% Off Wild Oats Cereal",
            subtitle: "Save 20% off any size Wild Oats breakfast cereal",
            expiration: <date time in JS format>
            image: <url to image>
            barcodeFormat: "IMAGE",
            barcodeValue: <url to barcode image>,
            termsAndConditions: [
                "Not valid with any other offer",
                "Limit one coupon per customer",
                "Participating retailers"
            ]
        }
    }


#### Login and use the app
Identical to the previous sequence except in this instance skip step 2
(creating the new user)


### Valid Currency Codes
For information regarding the use of currency codes in the system see
[User wallet](#markdown-header-user-wallet)

    "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN",
    "BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BRL",
    "BSD", "BTN", "BWP", "BYR", "BZD", "CAD", "CDF", "CHF", "CLP", "CNY",
    "COP", "CRC", "CUC", "CUP", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD",
    "EGP", "ERN", "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GGP", "GHS",
    "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF",
    "IDR", "ILS", "IMP", "INR", "IQD", "IRR", "ISK", "JEP", "JMD", "JOD",
    "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT",
    "LAK", "LBP", "LKR", "LRD", "LSL", "LTL", "LYD", "MAD", "MDL", "MGA",
    "MKD", "MMK", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MYR",
    "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB", "PEN",
    "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF",
    "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SLL", "SOS", "SPL",
    "SRD", "STD", "SVC", "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP",
    "TRY", "TTD", "TVD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS",
    "VEF", "VND", "VUV", "WST", "XAF", "XCD", "XDR", "XOF", "XPF", "YER",
    "ZAR", "ZMW", "ZWD",

    // Loyalty Point Codes
    "PBB"


### Recommended Profile Attribute Names
* Gender: <code>gender</code>
* Date of Birth: <code>dob</code>
* Social Security Number: <code>ssn</code>
* Ethnicity: <code>ethnicity</code>
* First Language: <code>firstLanguage</code>
* Religious Affiliation: <code>religiousAffiliation</code>
* Political Affiliation" <code>politicalAffiliation</code>
* Education Level: <code>educationLevel</code>
* Personal Email: <code>personal_email</code>
* Personal Mobile Number: <code>personal_mobilePhone</code>
* Household Income: <code>householdIncome</code>
* Gross Annual Income: <code>grossAnnualIncome</code>
* Credit Rating: <code>creditRating</code>
* Occupation: <code>occupation</code>
* Home DMA: <code>home_DMA</code>
* Home Residence Length: <code>home_residenceLength</code>
* Home Phone: <code>home_phone</code>
* Home Street1: <code>home_street1</code>
* Home Street2: <code>home_street2</code>
* Home City: <code>home_city</code>
* Home State: <code>home_state</code>
* Home Zip: <code>home_zip</code>
* Adult Count in Household: <code>adultCountInHousehold</code>
* Minor Count in Household: <code>minorCountInHousehold</code>
* Senior Count in Household: <code>seniorCountInHousehold</code>
* Pet Count in Household: <code>petCountInHousehold
* Work DMA: <code>work_DMA</code>
* Work Phone1: <code>work_phone1</code>
* Work Street1: <code>work_street1</code>
* Work Street2: <code>work_street2</code>
* Work City: <code>work_city</code>
* Work State: <code>work_state</code>
* Work Zip: <code>work_zip</code>
